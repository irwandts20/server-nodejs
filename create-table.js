var db = require("./db_config");

db.connect(function(err) {
    if (err) throw err;
    let sql = `CREATE TABLE customer_bank (
                cust_id INT (11) NOT NULL AUTO_INCREMENT,
                nama VARCHAR (50) NOT NULL,
                alamat VARCHAR (200) NOT NULL,
                kode_pos CHAR(5),
                no_hp VARCHAR (15),
                email VARCHAR (50),
                PRIMARY KEY (cust_id)
               )`;
    db.query(sql, function(err, result) {
        if (err) throw err;
        console.log("Table created");
    });
});